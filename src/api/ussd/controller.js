const {
  sendSuccessResponse,
  sendErrorResponse,
  getTopUpRequestData,
} = require("../../helpers/utility");
const { parseUssdContent } = require("../../helpers/utils/ussd");
const { getActiveTruckPoolByMobile } = require("../../services/truck");
const { getActiveTripByMobile } = require("../../services/trip");
const redis = require("../../helpers/redis_helper");
const message = require("../../helpers/content");
const { topUpRequest } = require("../../services/airtime");
const {
  delegateBeginEventForTruckRequestPool,
  delegateBeginEventForTrip,
  delegateContinueEventForTrip,
  delegateContinueEventForTruckRequestPool,
  delegateUssdOptionBeginEvent,
} = require("../../helpers/delegate");
const UssdRequestRepository = require("../../repositories/ussd_request.repository");
exports.onlineModeAction = async (req, res) => {
  const body = req.body;
  const event = !body.event ? "" : body.event;
  const token = req.header("Authorization").split(" ")[1];

  const key = "ussdapp:" + body.msisdn + ":" + body.session_id;
  let response;
  let params = {};
  if (event == "begin") {
    if (body.ussd_option && body.ussd_option !== "") {
      response = await delegateUssdOptionBeginEvent(body, token);
    } else {
      response = {
        content: message.menu,
        display: "dialogue",
        success: true,
      };
    }
  }
  if (response.success) {
    return sendSuccessResponse(
      res,
      {
        content: await parseUssdContent(response.content, params),
        display: response.display,
        session_id: body.session_id,
      },
      message.messageAcceptedForProcessing
    );
  }
};
//       const truckRequestPool = await getActiveTruckPoolByMobile(
//         body.msisdn,
//         token
//       );
//       if (truckRequestPool) {
//         response = await delegateBeginEventForTruckRequestPool(
//           key,
//           truckRequestPool
//         );
//       } else {
//         const trip = await getActiveTripByMobile(body.msisdn, token);
//         response = await delegateBeginEventForTrip(key, trip);
//       }
//     }
//   } else if (event == "continue") {
//     const data = await redis.get_data(key);
//     if (data) {
//       if (data.type == "trip") {
//         response = await delegateContinueEventForTrip(key, data, token, body);
//       } else if (data.type == "truckRequestPool") {
//         response = await delegateContinueEventForTruckRequestPool(
//           key,
//           data,
//           token,
//           body
//         );
//       } else {
//         return sendErrorResponse(res, {}, message.messageAcceptedForProcessing);
//       }
//     } else {
//       return sendErrorResponse(res, {}, message.messageAcceptedForProcessing);
//     }
//   } else {
//     return sendErrorResponse(res, {}, message.messageAcceptedForProcessing);
//   }

//   if (
//     response.display === "terminate" &&
//     response.success &&
//     response.content !== message.invalidInput &&
//     response.params &&
//     Object.keys(response.params).length !== 0 &&
//     checkCountry(response.params)
//   ) {
//     const topUpRequestData = getTopUpRequestData(response.params);
//     topUpRequest(topUpRequestData, token);
//   }

//   params = response.params ? response.params : {};

//   if (response.success) {
//     return sendSuccessResponse(
//       res,
//       {
//         content: await parseUssdContent(response.content, params),
//         display: response.display,
//         session_id: body.session_id,
//       },
//       message.messageAcceptedForProcessing
//     );
//   }

//   return sendErrorResponse(res, {}, message.messageAcceptedForProcessing);
// };

exports.offlineModeAction = async (req, res) => {
  const body = req.body;

  let content,
    display = "dialog";
  const event = !body.event ? "" : body.event;

  switch (event) {
    default:
    case "begin":
      content = "Welcome to Kobo360 application";
      display = "terminate";
      break;
    case "continue":
      if (body.menu_page == 2) {
        switch (body.message) {
          case "1":
            content = "What is your name";
            break;
          case "2":
            content = "Enter your pick up location";
            break;
        }
      } else {
        content = "Thanks for checking out this demo";
        display = "terminate";
      }
      break;
  }

  sendSuccessResponse(
    res,
    {
      content: await parseUssdContent(content, {}),
      display: display,
      session_id: body.session_id,
    },
    "MESSAGE_WAS_ACCEPTED_FOR_PROCESSING"
  );
};

exports.storeUssdRequest = async (req, res) => {
  const body = req.body;
  let ussdRequest;
  try {
    ussdRequest = await UssdRequestRepository.create(body);
  } catch (err) {
    console.log(err);
    sendSuccessResponse(res, {}, "FAILURE");
  }
  sendSuccessResponse(res, ussdRequest, "SUCCESS");
};

const checkCountry = (data) => {
  const ngn = "nigeria";
  if (data.requestCountry && data.requestCountry.toLowerCase() === ngn) {
    return true;
  }

  if (data.country && data.country.toLowerCase() === ngn) {
    return true;
  }

  return false;
};
