let router = require('express').Router();
let controller = require('./controller');
const {authenticate} = require('../../middlewares/middlewares');
const {ussdActionRequest} = require('../../middlewares/requests');

router.post('/online', [authenticate, ussdActionRequest], controller.onlineModeAction);
router.post('/offline', ussdActionRequest, controller.offlineModeAction);
router.post('/ussd_request', controller.storeUssdRequest);

module.exports = router;
