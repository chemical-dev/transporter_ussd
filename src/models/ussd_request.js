"use strict";
var mongoose = require("mongoose"),
    Schema = mongoose.Schema;

let UssdRequestSchema = new Schema({
    ussd_content: String,
    msisdn: String,
    short_code: String,
    operator: String,
    session_id: String,
    timestamp: String,
    request_type: String,
});
UssdRequestSchema.index({ "$**": "text" });
module.exports = mongoose.model("UssdRequest", UssdRequestSchema);