/**
 * Created by Adeyemi on 9/2/2018.
 */
const mongoose = require('mongoose'),
    config = require('../config/config');

const ENV = process.env.NODE_ENV;
let dbUrl = config.database.dbUrl.dev;
if (ENV === 'production') {
    dbUrl = encodeURI(config.database.dbUrl.prod);
}

const dbName = process.env.DB_NAME;

mongoose
    .connect(dbUrl, {
        dbName: dbName,
        // autoReconnect: true,
        useCreateIndex: true,
        useNewUrlParser: true,
        keepAlive: true,
        poolSize: 50,
        useUnifiedTopology: true,
    })
    .then((conn) => {
        console.log("DB Connected");
    })
    .catch((error) => {
        console.log("DB Error", error);
    });

//Make findOneAndUpdate() and findOneAndRemove() use native findOneAndUpdate() rather than findAndModify().
mongoose.set('useFindAndModify', false);

mongoose.connection.on('connected', function () {
    console.log('Mongoose connected to ' + config.dbUrl);
});
mongoose.connection.on('error', function (err) {
    console.log('Mongoose connection error: ' + err);
});
mongoose.connection.on('disconnected', function () {
    console.log('Mongoose disconnected');
});

// For nodemon restarts
process.once('SIGUSR2', function () {
    gracefulShutdown('nodemon restart', function () {
        process.kill(process.pid, 'SIGUSR2');
    });
});


// For app termination
process.on('SIGINT', function () {
    gracefulShutdown('app termination', function () {
        process.exit(0);
    });
});

// For Heroku app termination
process.on('SIGTERM', function () {
    gracefulShutdown('Heroku app shutdown', function () {
        process.exit(0);
    });
});

require('./ussd_request');
