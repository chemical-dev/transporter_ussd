const apiBase = '../api';

module.exports = function (app) {
    app.use('/', require(`${apiBase}/ussd/index`));
};
