const mongoose = require('mongoose');
const UssdRequest = mongoose.model('UssdRequest');

class UssdRequestRepository {

    constructor(ussdRequest) {
        this.ussdRequest = ussdRequest;
    }

    async create(data) {
        return this.ussdRequest.create(data);
    }
}

module.exports = new UssdRequestRepository(UssdRequest);
