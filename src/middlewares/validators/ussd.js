const Joi = require('@hapi/joi');

const ussdInput = (payload) => {
    const schema = Joi.object({
        msisdn: Joi.string().required(),
        session_id: Joi.string().required(),
        event: Joi.string().valid('begin', 'continue').required(),
        message: Joi.string().required(),
        operator: Joi.string().required(),
        menu_page: Joi.number().required()
    });
    return schema.validate(payload, {allowUnknown: true});
};

module.exports = {
    ussdInput
};
