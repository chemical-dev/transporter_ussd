const request = require('request');
const util = require('../helpers/utility');

exports.hasResource = (myRoles, strict) => {


    return (req, res, next) => {
        console.log('role middleware');

        myRoles = myRoles.toLowerCase();

        if(req.payload == undefined){
            console.log('pay load does not exist');
            return util.sendErrorResponse(res, [], "NO_PAYLOAD_PERMISSION_DENIED", 403);
        }

        const resources = req.payload.resources;

        // console.log(req.payload.resources);

        if(!resources.hasOwnProperty(myRoles)){
            console.log('role does not exist');
            return util.sendErrorResponse(res, [], "ROLE_NOT_SUPPORTED_PERMISSION_DENIED", 403);
        }

        const requiredRoles = req.payload.resources[myRoles];

        if( !(requiredRoles instanceof Array)){
            return util.sendErrorResponse(res, [], "UNKNOWN_ROLE", 401);
        }

        // console.log('role middleware');
        if(requiredRoles.length < 1){
            return next();
        }
        // console.log(req.query);
        // console.log(req.payload,"payload");
        if(req.payload && req.payload['userType']){
            const userType =  capitalize(req.payload['userType']);

            if (strict != undefined){
                if (strict.length > 0){
                    if(strict.includes(userType) == false){
                        console.log('pay load does not exist');
                        return util.sendErrorResponse(res, [], "INVALID_USER_TYPE_PERMISSION_DENIED", 403);
                    }
                }
            }


            console.log(userType);

            switch(userType){
                default:
                    if(requiredRoles.includes(userType)){
                        console.log('should continue');
                        return next();
                    }
                    break;
                case 'Admin':
                    if(req.payload.roles) {
                        const roles = req.payload.roles;
                        const found = requiredRoles.some( r => roles.some( x => r.toLowerCase() === x.toLowerCase()));
                        if (found) {
                            return next();
                        }
                    }
                    break;
            }
        }
        return util.sendErrorResponse(res, [], "NOT_AUTHORIZED_PERMISSION_DENIED", 403);
    };
};

exports.authenticate = (req, res, next) => {
    // console.log(req.payload);
    if(req.payload){
        return next();
    }
    console.log('authenticating');
    if(req.header('Authorization')){
        const authToken = req.header('Authorization').split(' ')[1];
        const authUrl = process.env.AUTH_URL;
        const authPort = process.env.AUTH_PORT;
        const AppToken = req.header('Apptoken');
        let authResponse = '';
        // console.log(authToken);

        let authRequest = request.post({
            url: `${authUrl}/user/decodeToken`,
            form: {token: authToken},
            headers: {
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + AppToken
                     }

        }, function(error, response, body){
            if(error){
                console.log(error);
                return util.sendErrorResponse(res, [], "USER_AUTHENTICATION_FAILED", 401);
            }
        });

        authRequest.on('data', (data) => {
            authResponse += data;
        });

        authRequest.on('end', () => {
            console.log('token decoded ');
            try {
                let data = JSON.parse(authResponse);
                // console.log(data);
                if (data.success) {
                    console.log('success');
                    req.payload = data.data;
                    return next();
                }
            } catch (e) {
                //todo: log error to sentry
                console.log(e);
            }
            return util.sendErrorResponse(res, [], "BAD_EXPIRED_TOKEN", 401);
        })
    }else {
        return util.sendErrorResponse(res, [], "NOT_AUTHENTICATED", 401);
    }
};

exports.authenticateApp = (req, res, next) => {
    // console.log('authenticating');
    // console.log(req.headers);
    next();
};

function capitalize(str){
    if(str.length > 0){
        let temp = str.substr(1);
        str = str.charAt(0).toUpperCase() + temp;
    }
    return str;
}
