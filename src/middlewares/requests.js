const UssdValidator = require('./validators/ussd');

const util = require('../helpers/utility');

exports.ussdActionRequest = (req, res, next) => {
    req.body = util.trimCollection(req.body);
    const validated = UssdValidator.ussdInput(req.body);
    if (validated.error) {
        return util.handleError(validated.error.details[0].message, res);
    }
    return next();
};
