const request = require('request');

class HttpService{

    static get({ url, headers, queryParams, action }) {
        return new Promise((resolve, reject) => {
            request.get({
                qs: {
                    ...queryParams,
                    language:_language
                },
                url: url,
                headers: headers,
            }, (error, response, body) => {
                if (error) {
                    reject(`Error communicating with ${action}`);
                } else if (!response) {
                    reject(`No response received from ${action}`)
                }
                resolve(response);
            });
        });
    }

    static post({ url, headers, data, action, queryParams }) {
        return new Promise((resolve, reject) => {
            request.post({
                qs: queryParams || '',
                url: url,
                headers: headers,
                json: data,
                qs: {
                    language:_language
                },
            }, (error, response, body) => {
                if (error) {
                    reject(`Error communicating with ${action}`);
                } else if (!response) {
                    reject(`No response received from ${action}`)
                }
                resolve(response);
            });
        });
    }

    static put({ url, headers, data, action }) {
        return new Promise((resolve, reject) => {
            request.put({
                url: url,
                headers: headers,
                json: data,
                qs: {
                    language:_language
                },
            }, (error, response, body) => {
                if (error) {
                    reject(`Error communicating with ${action}`);
                } else if (!response) {
                    reject(`No response received from ${action}`)
                }
                resolve(response);
            });
        });
    }

    static delete({ url, headers, data, action }) {
        return new Promise((resolve, reject) => {
            request.delete({
                url: url,
                headers: headers,
                json: data,
                qs: {
                    language:_language
                },
            }, (error, response, body) => {
                if (error) {
                    reject(`Error communicating with ${action}`);
                } else if (!response) {
                    reject(`No response received from ${action}`)
                }
                resolve(response);
            });
        });
    }
}

module.exports = HttpService;
