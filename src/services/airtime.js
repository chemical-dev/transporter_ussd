
const HttpService = require('./http');

const topUpRequest = async (body, token) => {
    const httpData = {
        url: `${process.env.AUTH_URL}/airtimeservice/topUp`,
        data: body,
        headers: {
            'Authorization': `Bearer ${token}`,
            'Content-Type': 'application/json'
        },
        action: 'TRIP'
    };
    const response = await HttpService.post(httpData);
    const responseBody = (typeof response.body !== 'object') ? JSON.parse(response.body) : response.body;
    if (!responseBody.success) {
        console.log(`Error occured while topping up for mobile: ${body[0].mobile}. Error: ${JSON.stringify(responseBody)}`);
        return null;
    }

    console.log(`Successfully topped up mobile: ${body[0].mobile}`);
    return responseBody.data;
};

module.exports = {
    topUpRequest
};
