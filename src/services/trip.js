const HttpService = require("./http");

const getActiveTripByMobile = async (msidn, token) => {
  const httpData = {
    url: `${process.env.AUTH_URL}/trip/active/mobile/${msidn}`,
    headers: {
      Authorization: `Bearer ${token}`,
      "Content-Type": "application/json",
    },
    action: "trip",
  };
  const response = await HttpService.get(httpData);
  const responseBody =
    typeof response.body !== "object"
      ? JSON.parse(response.body)
      : response.body;
  if (!responseBody.success) {
    console.log(
      `Error occured while getting trip for mobile: ${msidn}. Error: ${JSON.stringify(
        responseBody
      )}`
    );
    return null;
  }

  console.log(`Successfully got the trip for msisdn: ${msidn}`);
  return responseBody.data.trip;
};

const getActiveTripByRegNo = async (regNo, token) => {
  console.log(regNo, token);
  const httpData = {
    url: `${process.env.AUTH_URL}/trip/active/regNumber/${regNo}`,
    headers: {
      Authorization: `Bearer ${token}`,
      "Content-Type": "application/json",
    },
    action: "trip",
  };
  const response = await HttpService.get(httpData);
  // console.log(response.body);
  const responseBody =
    typeof response.body !== "object"
      ? JSON.parse(response.body)
      : response.body;
  //console.log(responseBody);
  //console.log(responseBody.success);
  if (!responseBody.success) {
    console.log(
      `Error occured while getting trip for mobile: ${msidn}. Error: ${JSON.stringify(
        responseBody
      )}`
    );

    return null;
  }
  // console.log(`Successfully got the trip for msisdn: ${msidn}`);
  //console.log(responseBody.trip);
  return responseBody.data.trip;
};

const updateTripStatus = async (id, token, status) => {
  const appToken = process.env.APP_TOKEN;
  const httpData = {
    url: `${process.env.AUTH_URL}/trip/${id}/updateStatus`,
    data: { status: status },
    headers: {
      Authorization: `Bearer ${token}`,
      Apptoken: appToken,
      "Content-Type": "application/json",
    },
    action: "trip",
  };
  const response = await HttpService.put(httpData);
  const responseBody =
    typeof response.body !== "object"
      ? JSON.parse(response.body)
      : response.body;
  if (!responseBody.success) {
    console.log(
      `Error occured while updating Trip status for TripId: ${id}. Error: ${JSON.stringify(
        responseBody
      )}`
    );
    throw `There was a problem while updating status to ${status}. Please Try Again.`;
  }

  console.log(`Successfully updated the trip status for tripId: ${id}`);
  return responseBody.data.trip;
};

const cancelTrip = async (id, token, comment) => {
  const httpData = {
    url: `${process.env.AUTH_URL}/trip/${id}`,
    data: { reason: comment },
    headers: {
      Authorization: `Bearer ${token}`,
      "Content-Type": "application/json",
    },
    action: "trip",
  };
  const response = await HttpService.delete(httpData);
  const responseBody =
    typeof response.body !== "object"
      ? JSON.parse(response.body)
      : response.body;
  if (!responseBody.success) {
    console.log(
      `Error occured while cancelling Trip for TripId: ${id}. Error: ${JSON.stringify(
        responseBody
      )}`
    );
    throw `There was a problem while cancelling trip. Please Try Again.`;
  }

  console.log(`Successfully cancelled the trip status for tripId: ${id}`);
  return responseBody.data;
};

const addCommunicationToTrip = async (id, token, status) => {
  const httpData = {
    url: `${process.env.AUTH_URL}/trip/${id}/addCommunicationUpdate`,
    data: {
      status: status,
      comment: "update from ussd app",
      person: "Driver",
      priority: "HIGH",
      tag: "incident",
    },
    headers: {
      Authorization: `Bearer ${token}`,
      "Content-Type": "application/json",
    },
    action: "trip",
  };
  const response = await HttpService.put(httpData);
  const responseBody =
    typeof response.body !== "object"
      ? JSON.parse(response.body)
      : response.body;
  if (!responseBody.success) {
    console.log(
      `Error occured while adding communication to Trip for TripId: ${id}. Error: ${JSON.stringify(
        responseBody
      )}`
    );
    throw `There was a problem while adding communication to Trip. Please Try Again.`;
  }

  console.log(`Successfully added communication to trip for tripId: ${id}`);
  return responseBody.data;
};

const reportIncident = async (body, token) => {
  //console.log(body);
  const httpData = {
    url: `${process.env.AUTH_URL}/message/createIncidentReport`,
    data: body,
    headers: {
      Authorization: `Bearer ${token}`,
      "Content-Type": "application/json",
    },
    action: "TRIP",
  };
  const response = await HttpService.post(httpData);
  //console.log(response);
  console.log("11111111111111");
  const responseBody =
    typeof response.body !== "object"
      ? JSON.parse(response.body)
      : response.body;
  console.log(responseBody.success);
  console.log("22222222222222");
  if (!responseBody) {
    console.log("33333333333333");
    console.log(
      `Error occured while topping up for mobile: ${
        body[0].mobile
      }. Error: ${JSON.stringify(responseBody)}`
    );
    console.log("44444444444444");
    return "could not post incident";
  }

  //console.log(responseBody.data);
  return responseBody.data;
};

module.exports = {
  getActiveTripByMobile,
  updateTripStatus,
  cancelTrip,
  addCommunicationToTrip,
  getActiveTripByRegNo,
  reportIncident,
};
