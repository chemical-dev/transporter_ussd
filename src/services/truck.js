const HttpService = require('./http');

const getActiveTruckPoolByMobile = async (msidn, token) => {
    const httpData = {
        url: `${process.env.AUTH_URL}/truck/truckRequestPool/mobile/${msidn}`,
        headers: {
            'Authorization': `Bearer ${token}`,
            'Content-Type': 'application/json'
        },
        action: 'trip'
    };
    const response = await HttpService.get(httpData);
    const responseBody = (typeof response.body !== 'object') ? JSON.parse(response.body) : response.body;
    
    const responseData = responseBody.data;
    if (!responseBody.success || !responseData.truckRequestPool) {
        console.log(`Error occured while getting truck for mobile: ${msidn}. Error: ${JSON.stringify(responseBody)}`);
        return null;
    }
    
    console.log(`Successfully got the truck for msisdn: ${msidn}`);
    return responseData.truckRequestPool;
};

const updateTruckRequestPoolStatus = async (id, token, status) => {
    const httpData = {
        url: `${process.env.AUTH_URL}/truck/${id}/updatePoolStatus`,
        data: {status: status},
        headers: {
            'Authorization': `Bearer ${token}`,
            'Content-Type': 'application/json'
        }
    };
    const response = await HttpService.put(httpData);
    const responseBody = (typeof response.body !== 'object') ? JSON.parse(response.body) : response.body;
    
    const responseData = responseBody.data;
    if (!responseBody.success) {
        console.log(`Error occured while updating truck status for truckId: ${id}. Error: ${JSON.stringify(responseBody)}`);
        throw (`There was a problem while updating status to ${status}. Please Try Again.`);
    }
    
    console.log(`Successfully updated the truck status for truckId: ${id}`);
    return responseData.truckRequestPool;
};

const pulloutTruckRequestPool = async (id, token) => {
    const httpData = {
        url: `${process.env.AUTH_URL}/truck/${id}/pullout`,
        headers: {
            'Authorization': `Bearer ${token}`,
            'Content-Type': 'application/json'
        }
    };
    const response = await HttpService.put(httpData);
    const responseBody = (typeof response.body !== 'object') ? JSON.parse(response.body) : response.body;
    
    const responseData = responseBody.data;
    if (!responseBody.success) {
        console.log(`Error occured while pulling out truck for truckId: ${id}. Error: ${JSON.stringify(responseBody)}`);
        throw (`There was a problem while withdrawing truck from pool. Please Try Again.`);
    }
    
    console.log(`Successfully pulledout truck truckId: ${id}`);
    return responseData.truckRequestPool;
};




module.exports = {
    getActiveTruckPoolByMobile,
    updateTruckRequestPoolStatus,
    pulloutTruckRequestPool
};
