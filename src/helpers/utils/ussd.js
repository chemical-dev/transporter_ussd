const Language = require('@shypes/language-translator');
Language._({
    __basedir : process.env.PWD,
    langFolder : 'src/lang/content'
});

const parseUssdContent = async (content, data) => {
    Language.setActiveLang(_language);
    Language.setLanguageDir('src/lang/content');

    let _content = ''; 

    if(typeof content == 'object'){
        if (content.message && typeof content.message == 'string'){
            _content += await Language.get(content.message, _language, data)+" \n";
        }
    
        if (content.options && typeof content.options == 'object'){
            for (x in content.options){
                item = content.options[x];
                let text = await Language.get(item['text'], _language, data);
                _content += item['key'] + "." + text + " \n";
            }
        }
    }

    if(typeof content == 'string'){
        _content = await Language.get(content, _language, data)+" \n";
    }
  
    return _content;
};

module.exports = {
    parseUssdContent
};
