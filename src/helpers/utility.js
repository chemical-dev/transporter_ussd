const Sentry = require('../../sentry');
const redis = require('./redis_helper');
redis.init();

const Language = require("@shypes/language-translator");
Language._({
    __basedir : process.env.PWD,
    langFolder : 'src/lang'
});

const sendErrorResponse = async (res, content, message, status) => {
    Language.setActiveLang(_language);
    Language.setLanguageDir('src/lang/responses');
    status = !status ? 400 : status;
    let responseData = {'message' : '', 'param' : ''};
    if(typeof message == 'string'){
        responseData['message'] = message;
        message = {};
    }
    responseData = {...responseData, ...message};
    let translated;
    if(responseData['message'] == 'MISSING_REQUIRED_FIELDS') {
        const msgStrings = [];
        for (i = 0; i < content.required.length; i++) {
            let msg = await Language.get(content.required[i].message, res.language, content.required[i].param);
            msgStrings.push(msg);
        }
        content.required =  msgStrings;
        translated = [msgStrings.slice(0, -1).join(', '), msgStrings.slice(-1)[0]].join(msgStrings.length < 2 ? '' : ' and ');
    } else {
        translated = await Language.get(responseData['message'], res.language, responseData['param']);
    }
    langKey = !(res.langKey == "true" || res.langKey == "1") ? false : true;
    responseKey = {
        'data': langKey ? Language.text('DATA') : 'data',
        'message': langKey ? Language.text('MESSAGE') : 'message',
        'success': langKey ? Language.text('SUCCESS') : 'success'
    }
    let data = {}
    data[responseKey['success']] =  false;
    data[responseKey['message']] =  translated;
    data[responseKey['data']] =  content;
    res.status(status).json(data);
};

const sendSuccessResponse = async (res, content, message) => {
    Language.setActiveLang(_language);
    Language.setLanguageDir('src/lang/responses');
    let responseData = {'message' : '', 'param' : ''};
    if(typeof message == 'string'){
        responseData['message'] = message;
        message = {};
    }
    responseData = {...responseData, ...message};
    let translated = await Language.get(responseData['message'], res.language, responseData['param']);
    langKey = !(res.langKey == "true" || res.langKey == "1") ? false : true;
    responseKey = {
        'data': langKey ? Language.text('DATA') : 'data',
        'message': langKey ? Language.text('MESSAGE') : 'message',
        'success': langKey ? Language.text('SUCCESS') : 'success'
    }
    let data = {}
    data[responseKey['success']] =  true;
    data[responseKey['message']] =  translated;
    data[responseKey['data']] =  content;
    res.status(200).json(data);
};

const trimCollection = (data) => {
    for(let key in data){
        if(data.hasOwnProperty(key)){
            if(typeof data[key] == 'string'){
                data[key] = data[key].trim();
            }
        }
    }
    return data;
};

const handleError = (err, res) => {
    console.error('Error: ', err);
    if (typeof err === 'string') {
        Sentry.captureMessage(err);
    } else {
        Sentry.captureException(err);
    }
    return sendErrorResponse(res, {}, 'MESSAGE_WAS_ACCEPTED_FOR_PROCESSING');
};

const saveDataToRedis = (key, data) => {
    redis.save_data(key, data, (err, reply) => {
        redis.expire(key, 300);
    });
};

const getTopUpRequestData = (data) => {
    const requestData = [];
    const request = {};
    request.mobile = data.driver.mobile;
    request.amount = 10;
    request.comment = `Top up for driver taking action : ${data.ussd_action ? data.ussd_action : ''}`;
    request.actionId = data._id;
    request.action = "TRIP";
    request.driverName = data.driver.name;
    request.driverId = data.driver.id;
    request.tripReadId = data.tripId;
    request.country = data.ussd_country.toUpperCase();
    switch(request.country){
        case 'NIGERIA':
            request.currency = 'NGN';
            break;
        default:
            request.currency = 'NGN';
    }
    requestData.push(request);
    return requestData;
};

module.exports = {
    sendSuccessResponse,
    sendErrorResponse,
    trimCollection,
    handleError,
    saveDataToRedis,
    getTopUpRequestData
};
