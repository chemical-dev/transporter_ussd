exports.awaitTruck = {
  message: `AWAIT_QUERY`,
  options: [
    {
      text: "AT_LOADING_STATION",
      key: "1",
    },
    {
      text: "WITHDRAW_TRUCK",
      key: "2",
    },
    {
      text: "EXIT",
      key: "0",
    },
  ],
};

exports.awaitTrip = {
  message: `AWAIT_QUERY`,
  options: [
    {
      text: "AT_LOADING_STATION",
      key: "1",
    },
    {
      text: "EXIT",
      key: "0",
    },
  ],
};

exports.startTrip = {
  message: `ONGOING_TRIP`,
  options: [
    {
      text: "TO_START_TRIP",
      key: "1",
    },
    {
      text: "TRIP_INFO",
      key: "2",
    },
  ],
};

exports.endTrip = {
  message: `ONGOING_TRIP`,
  options: [
    {
      text: "TO_END_TRIP",
      key: "1",
    },
    {
      text: "TRIP_INFO",
      key: "2",
    },
  ],
};

exports.reportIncident = {
  message: `INCIDENT_TEXT`,
  options: [
    {
      text: "ACCIDENT",
      key: "1",
    },
    {
      text: "FAULTY_TRUCK",
      key: "2",
    },
    {
      text: "REJECTED_GOODS",
      key: "3",
    },
    {
      text: "EXIT",
      key: "0",
    },
  ],
};

exports.tripInfo = {
  message: `ACTIVE_TRIP`,
  options: [
    {
      text: "TRIP_DESTINATION_INFO_KEY",
      key: "1",
    },
    {
      text: "REPORT_INCIDENT",
      key: "2",
    },
    {
      text: "TRIP_HELP_LINE_KEY",
      key: "0",
    },
  ],
};

exports.truckInfo = {
  message: `ACTIVE_TRUCK`,
  options: [
    {
      text: "TRUCK_DESTINATION_INFO",
      key: "1",
    },
    {
      text: "TRUCK_HELP_LINE",
      key: "0",
    },
  ],
};

exports.pendingTruckRequestPool = {
  message: `REPLY_WITH`,
  options: [
    {
      text: "ACCEPT_TRUCK_POOL",
      key: "1",
    },
    {
      text: "CANCEL_TRUCK_POOL",
      key: "0",
    },
  ],
};

exports.menu = {
  message: `Welcome to Kobo!`,
  options: [
    {
      text: "TRUCK REQUESTS",
      key: "1",
    },
    {
      text: "TRIP INFORMATION",
      key: "2",
    },
    {
      text: "REQUEST PAYMENT",
      key: "3",
    },
    {
      text: "REPORT INCIDENT",
      key: "4",
    },
    {
      text: "BUY DIESEL",
      key: "5",
    },
  ],
};

exports.menu3 = {
    message: `Welcome to Kobo!`,
    options: [
      {
        text: "TRUCK REQUESTS",
        key: "1",
      },
      {
        text: "TRIP INFORMATION",
        key: "2",
      },
      {
        text: "REQUEST PAYMENT",
        key: "3",
      },
      {
        text: "REPORT INCIDENT",
        key: "4",
      },
      {
        text: "BUY DIESEL",
        key: "5",
      },
    ],
  };
  

exports.menu2 = {
  message: `INPUT TRUCK REGISTRATION NUMBER`,
};

exports.notOnAnyTrip = "NOT_ON_ANY_TRIP";

exports.truckAtCustomerLocation = "TRUCK_AT_CUSTOMER_LOCATION";

exports.truckAtDestination = "TRUCK_AT_DESTINATION";

exports.truckStatusUpdated = "TRUCK_STATUS_UPDATED";
exports.tripStarted = "TRIP_STARTED";
exports.startedtrip = "STARTED_TRIP";
exports.tripEnded = "TRIP_ENDED";
exports.cancelled = "CANCELLED";
exports.cancelTrip = "CANCEL_TRIP";
exports.cancelTruck = "CANCEL_TRUCK";
exports.truckHelpline = "TRUCK_HELP_LINE";
exports.tripHelplineKey = "TRIP_HELP_LINE_KEY";
exports.tripHelpline = "TRIP_HELP_LINE";
exports.truckDestinationInfoKey = "TRUCK_DESTINATION_INFO_KEY";
exports.truckDestinationInfo = "TRUCK_DESTINATION_INFO";
exports.tripDestinationInfoKey = "TRIP_DESTINATION_INFO_KEY";
exports.tripDestinationInfo = "TRIP_DESTINATION_INFO";
exports.accidentReported = "ACCIDENT_REPORTED";
exports.faultyTruckReported = "FAULTY_TRUCK_REPORTED";
exports.rejectedGoodsReported = "REJECTED_GOODS_REPORTED";
exports.messageAcceptedForProcessing = "MESSAGE_WAS_ACCEPTED_FOR_PROCESSING";
exports.tripNotStarted = "TRIP_NOT_STARTED";
exports.truckRemoved = "TRUCK_REMOVED";
exports.comeBackLater = "COME_BACK_LATER";
exports.truckAtDestinationReachCustomer = "TRUCK_AT_DESTINATION_REACH_CUSTOMER";
exports.truckStatusSetToOffloaded = "TRUCK_STATUS_SET_TO_OFFLOADED";
exports.truckSetToReturningContainer = "TRUCK_SET_TO_RETURNING_CONTAINER";
exports.truckReturned = "TRUCK_RETURNED";
exports.containerTripsMessage = "DIAL_50_FOR_OFFLOADED_RETURNING_CONTAINER";
exports.invalidInput = "INVALID_INPUT";
exports.truckAccepted = "TRUCK_ACCEPTED";
exports.truckRequestCancelled = "TRUCK_REQUEST_CANCELLED";
exports.truckRequestPoolNotAccepted = "TRUCK_REQUEST_POOL_NOT_ACCEPTED";
exports.truckRequestAlreadyAccepted = "TRUCK_REQUEST_ALREADY_ACCEPTED";
exports.truckRequestPoolNotAcceptedDial =
  "TRUCK_REQUEST_POOL_NOT_ACCEPTED_DIAL_20_OR_30";
exports.tripFlagged = "TRIP_FLAGGED";

exports.rejectedGoods = {};
