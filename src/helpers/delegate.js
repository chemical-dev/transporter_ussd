const { updateTruckRequestPoolStatus } = require("../services/truck");
const { updateTripStatus } = require("../services/trip");
const message = require("./content");
const { saveDataToRedis } = require("./utility");
const {
  getActiveTripByMobile,
  addCommunicationToTrip,
  getActiveTripByRegNo,
  reportIncident,
} = require("../services/trip");
const {
  getActiveTruckPoolByMobile,
  pulloutTruckRequestPool,
} = require("../services/truck");
const { token } = require("morgan");
const { isEmpty } = require("underscore");

const delegateBeginEventForTrip = (key, trip) => {
  if (!trip) {
    return {
      content: message.notOnAnyTrip,
      display: "terminate",
      success: true,
    };
  }

  if (trip.flagged) {
    return {
      content: message.tripFlagged,
      display: "terminate",
      success: true,
      params: trip,
    };
  }

  let helpLineNo = "+2348186780000";
  if (
    trip.KoboBusinessUnitTag &&
    trip.KoboBusinessUnitTag.businessHead &&
    trip.KoboBusinessUnitTag.businessHead.phone
  ) {
    helpLineNo = trip.KoboBusinessUnitTag.businessHead.phone;
  }
  trip.ussd_helpLineNo = helpLineNo;
  trip.ussd_country = trip.requestCountry;

  switch (trip.status) {
    case "Available":
    case "Positioned":
      saveDataToRedis(key, {
        query: "awaitTripQuery",
        trip: JSON.stringify(trip),
        type: "trip",
      });
      return {
        content: message.awaitTrip,
        display: "dialog",
        success: true,
        params: trip,
      };
    case "In-premise":
      trip.ussd_action = "Trip In-premise query";
      return {
        content: message.truckAtCustomerLocation,
        display: "terminate",
        success: true,
        params: trip,
      };
    case "Loaded":
      saveDataToRedis(key, {
        query: "startTripQuery",
        trip: JSON.stringify(trip),
        type: "trip",
      });
      return {
        content: message.startTrip,
        display: "dialog",
        success: true,
        params: trip,
      };
    case "Transporting":
      saveDataToRedis(key, {
        query: "endTripQuery",
        trip: JSON.stringify(trip),
        type: "trip",
      });
      return {
        content: message.endTrip,
        display: "dialog",
        success: true,
        params: trip,
      };
    case "At-destination":
      let msg = message.truckAtDestination;
      if (trip.isContainer) {
        msg = message.containerTripsMessage;
      }
      trip.ussd_action = "Trip At-destination query";
      return {
        content: msg,
        display: "terminate",
        success: true,
        params: trip,
      };
    case "Offloaded":
    case "ReturningContainer":
      trip.ussd_action = "Trip Offloaded / ReturningContainer query";
      return {
        content: message.containerTripsMessage,
        display: "terminate",
        success: true,
        params: trip,
      };
    default:
      return {
        content: message.messageAcceptedForProcessing,
        display: "terminate",
        success: false,
        params: trip,
      };
  }
};

const delegateBeginEventForTruckRequestPool = (key, truckRequestPool) => {
  truckRequestPool.ussd_country = truckRequestPool.country;
  switch (truckRequestPool.status) {
    case "Pending":
      saveDataToRedis(key, {
        query: "pendingTruckRequestPoolQuery",
        truckRequestPool: JSON.stringify(truckRequestPool),
        type: "truckRequestPool",
      });
      return {
        content: message.pendingTruckRequestPool,
        display: "dialog",
        success: true,
        params: truckRequestPool,
      };
    case "Available":
    case "Positioned":
      data = {
        query: "awaitTruckRequestPoolQuery",
        truckRequestPool: JSON.stringify(truckRequestPool),
        type: "truckRequestPool",
      };
      saveDataToRedis(key, {
        query: "awaitTruckRequestPoolQuery",
        truckRequestPool: JSON.stringify(truckRequestPool),
        type: "truckRequestPool",
      });
      return {
        content: message.awaitTruck,
        display: "dialog",
        success: true,
        params: truckRequestPool,
      };
    case "In-premise":
      truckRequestPool.ussd_action = "Truck Request Pool In-premise query";
      return {
        content: message.truckAtCustomerLocation,
        display: "terminate",
        success: true,
        params: truckRequestPool,
      };
  }
};

const delegateUssdOptionBeginEvent = async (body, token) => {
  let data = {};
  let trip, tripResponse, trip1, tripInfo, stoke;
  try {
    trip = await getActiveTripByRegNo("549652GDI", token);
    if (body.ussd_option === "2" && trip) {
      // console.log(trip);
      tripResponse = {
        content: message.menu2,
        display: "dialog",
        success: true,
      };
      if (body.more_option) {
        trip1 = await getActiveTripByRegNo(body.more_option, token);
        console.log(trip1.status);

        tripInfo = `Customer Name: ${trip1.KoboBusinessUnitTag.parameters.customerName}\n`;
        tripInfo += `Pickup Location: ${trip1.pickupStation.address.slice(
          23,
          trip1.pickupStation.address.length
        )}\n`;
        tripInfo += `Delivery Location: ${trip1.deliveryStation.address.slice(
          22,
          trip1.deliveryStation.address.length
        )}\n`;
        tripInfo += `Current Location: ${trip1.statusHistory[0].address}`;
        tripResponse = {
          content: tripInfo,
          display: "dialog",
          success: true,
          params: trip1,
        };
      }
      return tripResponse;
    }
    if (body.ussd_option === "4") {
      trip1 = await getActiveTripByMobile(body.msisdn, token);
      console.log(trip1);
      stoke = {
        businessUnit: "Automation",
        country: "Nigeria",
        customerId: 1190,
        customerName: "Kobo Test Customer",
        deliveryStation: "Benin Sapele Road, Benin City, Nigeria",
        destination: "edo",
        email: "sanusi1@kobo360.com",
        from: "Sanusi Sulaiman",
        message: "mechanical breakdown",
        partnerId: 2055,
        partnerName: "Test Partner",
        pickupStation: "Apapa, Lagos, Nigeria",
        regNumber: "549652GDI",
        source: "lagos",
        status: "Accident",
        subject: "Attention Needed (Accident)",
        tag: "Accident",
        ticketType: "Incident",
        tripId: "TP1035656",
        truckNo: "549652GDI",
        userID: 25208,
        userType: "admin",
      };

      const report = await reportIncident(stoke, token);
      console.log(report, "iam here");
      //   if (report.data === isEmpty) {}

      tripResponse = {
        content: message.menu2,
        display: "dialog",
        success: true,
      };
      if (body.more_option === "1") {
        tripResponse = {
          content: "chillumbu1",
          display: "dialog",
          success: true,
        };
      }
      if (body.more_option === "2") {
        tripResponse = {
          content: "chillumbu2",
          display: "dialog",
          success: true,
        };
      }
      return tripResponse;
    }
    return {
      content: message.notOnAnyTrip,
      display: "terminate",
      success: false,
    };
  } catch (error) {}
};

// const delegateUssdOptionBeginEvent = async (body, token) => {
//   let data = {};
//   let truckRequestPool, trip;
//   try {
//     switch (body.ussd_option) {
//       case "10":
//         truckRequestPool = await getActiveTruckPoolByMobile(body.msisdn, token);

//         if (truckRequestPool) {
//           truckRequestPool.ussd_country = truckRequestPool.country;
//           switch (truckRequestPool.status) {
//             case "Pending":
//               await updateTruckRequestPoolStatus(
//                 truckRequestPool._id,
//                 token,
//                 "Available"
//               );
//               return {
//                 content: message.truckAccepted,
//                 display: "terminate",
//                 success: true,
//                 params: truckRequestPool,
//               };
//             default:
//               return {
//                 content: message.truckRequestAlreadyAccepted,
//                 display: "terminate",
//                 success: true,
//               };
//           }
//         } else {
//           return {
//             content: message.pendingTruckRequestPool,
//             display: "terminate",
//             success: true,
//           };
//         }
//       case "30":
//         truckRequestPool = await getActiveTruckPoolByMobile(body.msisdn, token);
//         if (truckRequestPool) {
//           truckRequestPool.ussd_country = truckRequestPool.country;
//           switch (truckRequestPool.status) {
//             case "Pending":
//               await updateTruckRequestPoolStatus(
//                 truckRequestPool._id,
//                 token,
//                 "Available"
//               );
//             case "Available":
//               await updateTruckRequestPoolStatus(
//                 truckRequestPool._id,
//                 token,
//                 "Positioned"
//               );
//             case "Positioned":
//               await updateTruckRequestPoolStatus(
//                 truckRequestPool._id,
//                 token,
//                 "In-premise"
//               );
//               truckRequestPool.ussd_action =
//                 "dialled short code 30 update status to In-premise";
//               return {
//                 content: message.truckStatusUpdated,
//                 display: "terminate",
//                 success: true,
//                 params: truckRequestPool,
//               };
//             case "In-premise":
//               truckRequestPool.ussd_action =
//                 "dialled short code 30 status In-premise";
//               return {
//                 content: message.truckAtCustomerLocation,
//                 display: "terminate",
//                 success: true,
//                 params: truckRequestPool,
//               };
//           }
//         } else {
//           trip = await getActiveTripByMobile(body.msisdn, token);
//           if (!trip) {
//             return {
//               content: message.notOnAnyTrip,
//               display: "terminate",
//               success: true,
//               params: trip,
//             };
//           }

//           trip.ussd_country = trip.requestCountry;
//           switch (trip.status) {
//             // case "Pending":
//             //     trip.ussd_action = "dialled short code 30 status Pending";
//             //     return {content: message.truckRequestPoolNotAcceptedDial, display: 'terminate', success: true, params: trip};
//             case "Pending":
//             case "Available":
//               await updateTripStatus(trip._id, token, "Positioned");
//             case "Positioned":
//               await updateTripStatus(trip._id, token, "In-premise");
//               trip.ussd_action =
//                 "dialled short code 30 updated status to In-premise";
//               return {
//                 content: message.truckStatusUpdated,
//                 display: "terminate",
//                 success: true,
//                 params: trip,
//               };
//             case "In-premise":
//               trip.ussd_action = "dialled short code 30 status In-premise";
//               return {
//                 content: message.truckAtCustomerLocation,
//                 display: "terminate",
//                 success: true,
//                 params: trip,
//               };
//             case "Loaded":
//               trip.ussd_action = "dialled short code 30 status Loaded";
//               return {
//                 content: message.tripNotStarted,
//                 display: "terminate",
//                 success: true,
//                 params: trip,
//               };
//             case "Transporting":
//               trip.ussd_action = "dialled short code 30 status Transporting";
//               return {
//                 content: message.tripStarted,
//                 display: "terminate",
//                 success: true,
//                 params: trip,
//               };
//             case "At-destination":
//               trip.ussd_action = "dialled short code 30 status At-destination";
//               return {
//                 content: message.truckAtDestinationReachCustomer,
//                 display: "terminate",
//                 success: true,
//                 params: trip,
//               };
//             case "Offloaded":
//             case "ReturningContainer":
//               trip.ussd_action =
//                 "dialled short code 30 status Offloaded / Returning container";
//               return {
//                 content: message.containerTripsMessage,
//                 display: "terminate",
//                 success: true,
//                 params: trip,
//               };
//             default:
//               trip.ussd_action = "dialled short code 30. Trip not started";
//               return {
//                 content: message.tripNotStarted,
//                 display: "terminate",
//                 success: true,
//                 params: trip,
//               };
//           }
//         }
//       case "40":
//         trip = await getActiveTripByMobile(body.msisdn, token);

//         if (!trip) {
//           return {
//             content: message.notOnAnyTrip,
//             display: "terminate",
//             success: true,
//             params: trip,
//           };
//         }

//         trip.ussd_country = trip.requestCountry;
//         switch (trip.status) {
//           case "Loaded":
//             await updateTripStatus(trip._id, token, "Transporting");
//             trip.ussd_action = "dialled short code 40. Trip started";
//             return {
//               content: message.tripStarted,
//               display: "terminate",
//               success: true,
//               params: trip,
//             };
//           case "Transporting":
//             trip.ussd_action = "dialled short code 40. Trip started";
//             return {
//               content: message.tripStarted,
//               display: "terminate",
//               success: true,
//               params: trip,
//             };
//           case "Pending":
//             trip.ussd_action =
//               "dialled short code 40. Truck request pool not accepted";
//             return {
//               content: message.truckRequestPoolNotAcceptedDial,
//               display: "terminate",
//               success: true,
//               params: trip,
//             };
//           case "Available":
//           case "Positioned":
//           case "In-premise":
//             trip.ussd_action =
//               "dialled short code 40. Truck at Customer location";
//             return {
//               content: message.truckAtCustomerLocation,
//               display: "terminate",
//               success: true,
//               params: trip,
//             };
//           case "At-destination":
//             trip.ussd_action = "dialled short code 40. Truck at Destination";
//             return {
//               content: message.truckAtDestinationReachCustomer,
//               display: "terminate",
//               success: true,
//               params: trip,
//             };
//           case "Offloaded":
//           case "ReturningContainer":
//             trip.ussd_action =
//               "dialled short code 40 status Offloaded / Returning container";
//             return {
//               content: message.containerTripsMessage,
//               display: "terminate",
//               success: true,
//               params: trip,
//             };
//           default:
//             return {
//               content: message.messageAcceptedForProcessing,
//               display: "terminate",
//               success: false,
//             };
//         }
//       case "50":
//         trip = await getActiveTripByMobile(body.msisdn, token);

//         if (!trip) {
//           return {
//             content: message.notOnAnyTrip,
//             display: "terminate",
//             success: true,
//             params: data,
//           };
//         }

//         trip.ussd_country = trip.requestCountry;
//         if (trip.isContainer) {
//           switch (trip.status) {
//             case "Pending":
//               trip.ussd_action =
//                 "dialled short code 50 status truck not accepted";
//               return {
//                 content: message.truckRequestPoolNotAcceptedDial,
//                 display: "terminate",
//                 success: true,
//                 params: trip,
//               };
//             case "Available":
//             case "Positioned":
//             case "In-premise":
//               trip.ussd_action =
//                 "dialled short code 50 status truck at customer location";
//               return {
//                 content: message.truckAtCustomerLocation,
//                 display: "terminate",
//                 success: true,
//                 params: trip,
//               };
//             case "Loaded":
//               trip.ussd_action =
//                 "dialled short code 50 status trip not started";
//               return {
//                 content: message.tripNotStarted,
//                 display: "terminate",
//                 success: true,
//                 params: trip,
//               };
//             case "Transporting":
//               await updateTripStatus(trip._id, token, "At-destination");
//               trip.ussd_action = "dialled short code 50 status trip ended";
//               return {
//                 content: message.tripEnded,
//                 display: "terminate",
//                 success: true,
//                 params: trip,
//               };
//             case "At-destination":
//               await updateTripStatus(trip._id, token, "Offloaded");
//               trip.ussd_action = "dialled short code 50 set to Offloaded";
//               return {
//                 content: message.truckStatusSetToOffloaded,
//                 display: "terminate",
//                 success: true,
//                 params: trip,
//               };
//             case "Offloaded":
//               await updateTripStatus(trip._id, token, "ReturningContainer");
//               trip.ussd_action =
//                 "dialled short code 50 set to ReturningContainer";
//               return {
//                 content: message.truckSetToReturningContainer,
//                 display: "terminate",
//                 success: true,
//                 params: trip,
//               };
//             case "ReturningContainer":
//               trip.ussd_action = "dialled short code 50. Truck returned";
//               return {
//                 content: message.truckReturned,
//                 display: "terminate",
//                 success: true,
//                 params: trip,
//               };
//             default:
//               trip.ussd_action = "dialled short code 50. trip not started";
//               return {
//                 content: message.tripNotStarted,
//                 display: "terminate",
//                 success: true,
//                 params: trip,
//               };
//           }
//         } else {
//           switch (trip.status) {
//             case "Pending":
//               trip.ussd_action = "dialled short code 50. status Pending";
//               return {
//                 content: message.truckRequestPoolNotAcceptedDial,
//                 display: "terminate",
//                 success: true,
//                 params: trip,
//               };
//             case "Available":
//             case "Positioned":
//             case "In-premise":
//               trip.ussd_action =
//                 "dialled short code 50. truck at customer location";
//               return {
//                 content: message.truckAtCustomerLocation,
//                 display: "terminate",
//                 success: true,
//                 params: trip,
//               };
//             case "Loaded":
//               trip.ussd_action = "dialled short code 50. trip not started";
//               return {
//                 content: message.tripNotStarted,
//                 display: "terminate",
//                 success: true,
//                 params: trip,
//               };
//             case "Transporting":
//               await updateTripStatus(trip._id, token, "At-destination");
//               trip.ussd_action =
//                 "dialled short code 50. update status to At-destination";
//               return {
//                 content: message.tripEnded,
//                 display: "terminate",
//                 success: true,
//                 params: trip,
//               };
//             case "At-destination":
//               trip.ussd_action = "dialled short code 50. truck at destination";
//               return {
//                 content: message.truckAtDestinationReachCustomer,
//                 display: "terminate",
//                 success: true,
//                 params: trip,
//               };
//             default:
//               trip.ussd_action = "dialled short code 50. trip not started";
//               return {
//                 content: message.tripNotStarted,
//                 display: "terminate",
//                 success: true,
//                 params: trip,
//               };
//           }
//         }
//       default:
//         return {
//           content: message.messageAcceptedForProcessing,
//           display: "terminate",
//           success: false,
//         };
//     }
//   } catch (err) {
//     return { content: err, display: "terminate", success: true };
//   }
// };

const delegateContinueEventForTrip = async (key, data, token, body) => {
  try {
    const trip = JSON.parse(data.trip);
    switch (data.query) {
      case "awaitTripQuery":
        switch (body.message) {
          case "1":
            trip.ussd_action = "update status to In-premise";
            if (trip.status == "Available") {
              await updateTripStatus(trip._id, token, "Positioned");
            }
            await updateTripStatus(trip._id, token, "In-premise");
            return {
              content: message.truckStatusUpdated,
              display: "terminate",
              success: true,
              params: trip,
            };
          case "0":
            trip.ussd_action = "exiting";
            return {
              content: message.comeBackLater,
              display: "terminate",
              success: true,
              params: trip,
            };
          default:
            return {
              content: message.invalidInput,
              display: "terminate",
              success: true,
              params: trip,
            };
        }
      case "startTripQuery":
        switch (body.message) {
          case "1":
            trip.ussd_action = "update status to Transporting";
            await updateTripStatus(trip._id, token, "Transporting");
            return {
              content: message.startedtrip,
              display: "terminate",
              success: true,
              params: trip,
            };
          case "2":
            data.query = "tripInfoQuery";
            saveDataToRedis(key, data);
            return {
              content: message.tripInfo,
              display: "dialog",
              success: true,
              params: trip,
            };
          default:
            return {
              content: message.invalidInput,
              display: "terminate",
              success: true,
              params: trip,
            };
        }
      case "endTripQuery":
        switch (body.message) {
          case "1":
            trip.ussd_action = "update status to At-destination";
            await updateTripStatus(trip._id, token, "At-destination");
            return {
              content: message.tripEnded,
              display: "terminate",
              success: true,
              params: trip,
            };
          case "2":
            data.query = "tripInfoQuery";
            saveDataToRedis(key, data);
            return {
              content: message.tripInfo,
              display: "dialog",
              success: true,
              params: trip,
            };
          default:
            return {
              content: message.invalidInput,
              display: "terminate",
              success: true,
              params: trip,
            };
        }
      case "tripInfoQuery":
        switch (body.message) {
          case "1":
            trip.ussd_action = "trip destination info";
            return {
              content: message.tripDestinationInfo,
              display: "terminate",
              success: true,
              params: trip,
            };
          case "2":
            data.query = "reportIncidentQuery";
            saveDataToRedis(key, data);
            return {
              content: message.reportIncident,
              display: "dialog",
              success: true,
              params: trip,
            };
          case "0":
            trip.ussd_action = "trip help line";
            return {
              content: message.tripHelpline,
              display: "terminate",
              success: true,
              params: trip,
            };
          default:
            return {
              content: message.invalidInput,
              display: "terminate",
              success: true,
              params: trip,
            };
        }
      case "reportIncidentQuery":
        switch (body.message) {
          case "1":
            trip.ussd_action = "adding accident communication";
            await addCommunicationToTrip(trip._id, token, "Accident");
            return {
              content: message.accidentReported,
              display: "terminate",
              success: true,
              params: trip,
            };
          case "2":
            trip.ussd_action = "adding faulty truck communication";
            await addCommunicationToTrip(trip._id, token, "Faulty Truck");
            return {
              content: message.faultyTruckReported,
              display: "terminate",
              success: true,
              params: trip,
            };
          case "3":
            trip.ussd_action = "adding rejected goods communication";
            await addCommunicationToTrip(trip._id, token, "Rejected Goods");
            return {
              content: message.rejectedGoodsReported,
              display: "terminate",
              success: true,
              params: trip,
            };
          case "0":
            trip.ussd_action = "exiting";
            return {
              content: message.comeBackLater,
              display: "terminate",
              success: true,
              params: trip,
            };
          default:
            return {
              content: message.invalidInput,
              display: "terminate",
              success: true,
              params: trip,
            };
        }
      default:
        return {
          content: message.messageAcceptedForProcessing,
          display: "terminate",
          success: false,
        };
    }
  } catch (err) {
    return { content: err, display: "terminate", success: true };
  }
};

const delegateContinueEventForTruckRequestPool = async (
  key,
  data,
  token,
  body
) => {
  try {
    const truckRequestPool = JSON.parse(data.truckRequestPool);
    switch (data.query) {
      case "awaitTruckRequestPoolQuery":
        switch (body.message) {
          case "1":
            if (truckRequestPool.status == "Available") {
              await updateTruckRequestPoolStatus(
                truckRequestPool._id,
                token,
                "Positioned"
              );
            }
            await updateTruckRequestPoolStatus(
              truckRequestPool._id,
              token,
              "In-premise"
            );
            truckRequestPool.ussd_action = "update status to In-premise";
            return {
              content: message.truckStatusUpdated,
              display: "terminate",
              success: true,
              params: truckRequestPool,
            };
          case "2":
            await pulloutTruckRequestPool(truckRequestPool._id, token);
            truckRequestPool.ussd_action = "pull out truck request pool";
            return {
              content: message.truckRemoved,
              display: "terminate",
              success: true,
              params: truckRequestPool,
            };
          case "0":
            truckRequestPool.ussd_action = "exiting";
            return {
              content: message.comeBackLater,
              display: "terminate",
              success: true,
              params: truckRequestPool,
            };
          default:
            return {
              content: message.invalidInput,
              display: "terminate",
              success: true,
              params: truckRequestPool,
            };
        }
      case "pendingTruckRequestPoolQuery":
        switch (body.message) {
          case "1":
            await updateTruckRequestPoolStatus(
              truckRequestPool._id,
              token,
              "Available"
            );
            truckRequestPool.ussd_action = "update status to Available";
            return {
              content: message.truckAccepted,
              display: "terminate",
              success: true,
              params: truckRequestPool,
            };
          case "0":
            await pulloutTruckRequestPool(truckRequestPool._id, token);
            truckRequestPool.ussd_action = "cancelling truck request pool";
            return {
              content: message.truckRequestCancelled,
              display: "terminate",
              success: true,
              params: truckRequestPool,
            };
          default:
            return {
              content: message.invalidInput,
              display: "terminate",
              success: true,
              params: truckRequestPool,
            };
        }
      default:
        return {
          content: message.messageAcceptedForProcessing,
          display: "terminate",
          success: false,
        };
    }
  } catch (err) {
    return { content: err, display: "terminate", success: true };
  }
};

module.exports = {
  delegateBeginEventForTrip,
  delegateBeginEventForTruckRequestPool,
  delegateContinueEventForTrip,
  delegateContinueEventForTruckRequestPool,
  delegateUssdOptionBeginEvent,
};
