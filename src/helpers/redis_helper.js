var _config = require('../config/config');

var redis_wrapper = {

    _error_message : "Connection to Service Gateway Failed!",

    client: null,

    connected: false,

    options : {
        host: '',
        port: '',
        db: '',
        password: '',
    },

    init : function(){

        var redis = require("redis");

        redis_wrapper.options = redis_wrapper.extend({}, redis_wrapper.options, {
            host: _config.database.redis.host,
            port: _config.database.redis.port,
            db: _config.database.redis.db,
            password: _config.database.redis.password,
        });

        redis_wrapper.client = redis.createClient(redis_wrapper.options);

        redis_wrapper.connected = true;
    },

    extend : function (target) {
        var sources = [].slice.call(arguments, 1);
        sources.forEach(function (source) {
            for (var prop in source) {
                target[prop] = source[prop];
            }
        });
        return target;
    },

    save_data : function (key, data, callback) {
        redis_wrapper.client.hmset(key, data, function (err, reply) {
            if (callback != undefined){
                return callback(err, reply);
            } 
        });
    },

    set_item : function (id, key, data, callback) {
        redis_wrapper.client.hset(id, key, data, function (err, reply) {
            if (callback != undefined){
                return callback(err, reply);
            } 
        });
    },

    get_item : function (id, key, callback) {
        redis_wrapper.client.hget(id, key, function (err, reply) {
            if (callback != undefined){
                return callback(err, reply);
            } 
        });
    },

    set : function (key, data, callback) {
        redis_wrapper.client.set(key, data, function (err, reply) {
            if (callback != undefined){
                return callback(err, reply);
            } 
        });
    },

    get : function (key, callback) {
        redis_wrapper.client.get(key, function (err, reply, data) {
            if (callback != undefined){
                return callback(err, reply, data);
            } 
        });
    },

    get_data : async (key) => {
        return new Promise((resolve, reject) => {
            redis_wrapper.client.hgetall(key, (err, reply) => {
                if(err){
                    reject(err);
                }
                resolve(reply);
            });
        });
    },

    expire : function (key, ttl, callback) {
        redis_wrapper.client.expire(key, ttl,function (err, reply) {
            if (callback != undefined){
                return callback(err, reply);
            } 
        });
    },

    delete : function (key, callback) {
        redis_wrapper.client.del(key,function (err, reply) {
            if (callback != undefined){
                return callback(err, reply);
            } 
        });
    },

    exist : function (key, callback) {
        redis_wrapper.client.exists(key,function (err, reply) {
            if (callback != undefined){
                return callback(err, reply);
            } 
        });
    },
}

module.exports = redis_wrapper;