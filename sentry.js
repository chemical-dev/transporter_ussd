const Sentry = require('@sentry/node');
const ENV = process.env.NODE_ENV;
if (ENV !== 'development') {
    Sentry.init({
        dsn: 'https://2ce2bcfb5f8b4e19872f21cad47df360@o222555.ingest.sentry.io/5312751',
        environment: ENV
    });
}

module.exports = Sentry;
