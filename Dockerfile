FROM mhart/alpine-node:10
MAINTAINER Adeyemi Adekunle <yemi@kobo360.com>

RUN cd /

RUN mkdir microservice

RUN cd microservice

WORKDIR /microservice

ADD . /microservice

RUN chmod +x /microservice/run.sh

RUN npm install

EXPOSE 80

CMD ./run.sh