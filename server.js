require('dotenv').load();
const Sentry = require('./sentry');
const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const compression = require('compression');
const cors = require('cors');
var tracingMiddlerware = require("./lib/tracing-middleware"); //jaeger tracing middleware

//Database Configuration
require('./src/models/db');

const indexRouter = require('./src/routes');
const usersRouter = require('./src/routes/users');

const app = express();

app.disable('x-powered-by');//turn off header

app.use(tracingMiddlerware()); //jaeger tracing

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

app.use(Sentry.Handlers.requestHandler());

app.use(compression());
app.use(logger('dev'));
app.use(express.json({extended: true}));
app.use(express.urlencoded({extended: true}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(cors());

app.options('*', cors());

app.use('/', indexRouter);
app.use('/users', usersRouter);

//Enable CORS from client side
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Methods', 'PUT,GET,DELETE,POST,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With,Content-Type, Accept, Authorization, Apptoken,' +
        ' Access-Control-Allow-Credential');
    res.header('Access-Control-Allow-Credentials', 'true');
    res.language = req.query.language || 'en';
    res.langKey = req.query.langKey || false;
    global._language = res.language;

    next();
});


/**
 * Api routing section
 */
require('./src/routes/api')(app);

// Log error to sentry
app.use(Sentry.Handlers.errorHandler());


// catch 404 and forward to error handler
app.use(function (req, res, next) {
    next(createError(404));
});

// Catch unauthorised errors
app.use(function (err, req, res, next) {
    if (err.name === 'UnauthorizedError') {
        res.status(401);
        res.json({"message": err.name + ": " + err.message});
    }
});

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;
